package array;

import interfaces.IList;
import java.util.Arrays;
import java.util.NoSuchElementException;


public class AList2<T> implements IList<T> {

    private T[] data;
    private int capacity;
    private int size;

    public AList2(T[] array) {
        if (array == null) {
            throw new IllegalArgumentException();
        }
        T[] buffer = Arrays.copyOf(array, array.length);
        this.data = buffer;
        this.capacity = buffer.length;
        this.size = buffer.length;
    }

    public AList2() {
        this(10);
    }

    public AList2(int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException();
        }
        this.capacity = capacity;
        this.data = (T[]) new Object[this.capacity];
        this.size = 0;
    }

    @Override
    public void clear() {
        for (int i = 0; i < data.length; i++) {
            data[i] = null;
        }
        size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public T get(int index) {
        if (index > (this.size - 1)) {
            throw new IndexOutOfBoundsException();
        }
        return data[index];
    }

    @Override
    public boolean add(T value) {
        return add(size, value);
    }

    @Override
    public boolean add(int index, T value) {
        if (index > size) {
            throw new IndexOutOfBoundsException();
        }
        if (size >= capacity) {
            resize();
        }
        System.arraycopy(data, index, data, index + 1, size - index);
        data[index] = value;
        size++;
        return true;
    }

    @Override
    public T remove(T value) {
        int indexOfValue = -1;
        for (int i = 0; i < size; i++) {
            if (data[i] == null) {
                if (value == null) {
                    indexOfValue = i;
                }
            } else {
                if (data[i].equals(value)) {
                    indexOfValue = i;
                }
            }
        }
        if (indexOfValue == -1) {
            throw new NoSuchElementException();
        } else {
            return removeByIndex(indexOfValue);
        }
    }

    @Override
    public T removeByIndex(int index) {
        T element = get(index);
        System.arraycopy(data, index + 1, data, index, size - index);
        data[size] = null;
        size--;
        return element;
    }

    @Override
    public boolean contains(T value) {
        for (int i = 0; i < size; i++) {
            if (data[i] == null) {
                if (value == null) {
                    return true;
                } else {
                    continue;
                }
            }
            if (data[i].equals(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean set(int index, T value) {
        if (index >= size) {
            return false;
        }
        data[index] = value;
        return true;
    }

    @Override
    public void print() {
        System.out.println(generateLine());
    }

    @Override
    public T[] toArray() {
        return Arrays.copyOf(data, size);
    }

    @Override
    public boolean removeAll(T[] arr) {
        if (arr == null) {
            return false;
        }
        for (int i = 0; i < arr.length; i++) {
            if (contains(arr[i])) {
                remove(arr[i]);
            }
        }
        return true;
    }

    private void resize() {
        int newLength = ((capacity * 3) / 2) + 1;
        T[] newArray = (T[]) new Object[newLength];
        System.arraycopy(data, 0, newArray, 0, size);
        data = newArray;
        capacity = newLength;
    }

    private String generateLine() {
        if (data == null) {
            return "null";
        }

        int iMax = size - 1;
        if (iMax == -1) {
            return "[]";
        }

        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; ; i++) {
            b.append(String.valueOf(data[i]));
            if (i == iMax) {
                return b.append(']').toString();
            }
            b.append(", ");
        }
    }
}
