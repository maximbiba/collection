package linked;

import interfaces.IList;
import java.util.Arrays;

public class LList1<T> implements IList<T> {

    private Node1<T> first;
    private Node1<T> last;
    private int size;

    public LList1() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    public LList1(T[] array) {
        if (array == null) {
            throw new IllegalArgumentException();
        }
        T[] buffer = Arrays.copyOf(array, array.length);
        for (int i = 0; i < buffer.length; i++) {
            add(buffer[i]);
        }
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public T get(int index) {
        if ((size < 1) || (size <= index) || (index < 0)) {
            throw new IndexOutOfBoundsException();
        }
        Node1<T> x = first;
        if (index == 0) {
            return x.object;
        }
        for (int i = 0; i < index; i++) {
            x = x.next;
        }

        return x.object;
    }

    @Override
    public boolean add(T value) {
        return addLast(value);
    }

    @Override
    public boolean add(int index, T value) {
        if (index < 0 || index > size) {
            return false;
        }

        if (index == size) {
            return addLast(value);
        }

        if (index == 0) {
            return addFirst(value);
        }

        Node1<T> current = getNodeInIndex(index - 1);
        Node1<T> afterCurrent = current.next;
        Node1<T> newNode = new Node1<>(value, afterCurrent);
        current.next = newNode;
        size++;
        return true;
    }

    @Override
    public T remove(T value) {
        int index = indexOf(value);
        return removeByIndex(index);
    }

    @Override
    public T removeByIndex(int index) {
        if (index < 0 || index >= size) {
            return null;
        }

        T objInIndex;

        if (index == 0) {
            objInIndex = first.object;
            first = first.next;
            size--;
            return objInIndex;
        }

        Node1<T> nodeInIndex = getNodeInIndex(index);
        Node1<T> previousNode = getNodeInIndex(index - 1);
        Node1<T> nextNode = nodeInIndex.next;

        previousNode.next = nextNode;
        size--;
        return nodeInIndex.object;
    }

    @Override
    public boolean contains(T value) {
        return indexOf(value) != -1;
    }

    @Override
    public boolean set(int index, T value) {
        if (index < 0 || index >= size || size == 0) {
            return false;
        }
        getNodeInIndex(index).object = value;
        return true;
    }

    @Override
    public void print() {
        System.out.println(generateLine());
    }

    @Override
    public T[] toArray() {
        T[] rez = (T[]) new Object[size];
        if (size < 1) {
            return rez;
        }
        Node1<T> x = first;
        for (int i = 0; i < size; i++) {
            rez[i] = x.object;
            x = x.next;
        }
        return rez;
    }

    @Override
    public boolean removeAll(T[] arr) {
        if (arr == null) {
            return false;
        }
        for (int i = 0; i < arr.length; i++) {
            remove(arr[i]);
        }
        return true;
    }

    public T getFirst() {
        if (first == null) {
            return null;
        }
        return first.object;
    }

    public T getLast() {
        if (last == null) {
            return null;
        }
        return last.object;
    }

    private boolean addLast(T value) {
        Node1<T> l = last;
        Node1<T> newNode = new Node1<>(value, null);
        last = newNode;
        if (l == null) {
            first = newNode;
        } else {
            l.next = newNode;
        }
        size++;
        return true;
    }

    private Node1<T> getNodeInIndex(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }
        Node1<T> nodeInIndex = first;

        for (int i = 0; i < index; i++) {
            nodeInIndex = nodeInIndex.next;
        }

        return nodeInIndex;
    }

    private boolean addFirst(T value) {
        Node1<T> newNode = new Node1<>(value, first);
        first = newNode;
        size++;
        return true;
    }

    private int indexOf(T obj) {
        int index = 0;
        if (obj == null) {
            for (Node1<T> x = first; x != null; x = x.next) {
                if (x.object == null) {
                    return index;
                }
                index++;
            }
        } else {
            for (Node1<T> x = first; x != null; x = x.next) {
                if (obj.equals(x.object)) {
                    return index;
                }
                index++;
            }
        }
        return -1;
    }

    private String generateLine() {
        int iMax = size - 1;
        if (iMax == -1) {
            return "[]";
        }

        Node1<T> x = first;

        StringBuilder b = new StringBuilder();
        b.append('[');

        for (int i = 0; ; i++) {
            b.append(String.valueOf(x.object));
            if (i == iMax) {
                return b.append(']').toString();
            }
            b.append(", ");
            x = x.next;
        }
    }
}
